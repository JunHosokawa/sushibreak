﻿unit USushiBreak;

{
  ORIGINAL SOURCE: https://gist.github.com/lynatan/217dbbf0de4db81de60c279eb3171039
  著作権は @Lynatan さんにあります。
}

interface

uses
  System.Classes, Vcl.Controls, Vcl.Graphics, Vcl.Forms, ToolsAPI;

type
  TSushiBreak = class
  private const
    // TImageList内のインデックスはこちらを参照: http://imgur.com/wur8hIn
    BREAK_IMAGE_INDEX = 0;
    // 1行の高さが15pxより小さい環境では'LargeGutterImages'の代わりに'SmallGutterImages'を使用する
    IMAGE_LIST_NAME = 'LargeGutterImages';
    // IMAGE_LIST_NAME = 'SmallGutterImages';
  private class var
    FImages: TImageList;
    FOrgImage: TBitmap;
  private
    class function Replace(const Bmp: TBitmap): Boolean;
  public
    class function Register(const Bmp: TBitmap): Boolean;
    class function Unregister: Boolean;
  end;

implementation

{ TSushiBreak }

class function TSushiBreak.Register(const Bmp: TBitmap): Boolean;
var
  envOptions: TComponent;
begin
  Result := False;

  if (Bmp = nil) or (Bmp.Empty) then
    Exit;

  envOptions := Application.FindComponent('EnvironmentOptions');
  if envOptions = nil then
    Exit;

  FImages := TImageList(envOptions.FindComponent(IMAGE_LIST_NAME));
  if FImages = nil then
    Exit;

  FOrgImage := TBitmap.Create;
  FImages.GetBitmap(BREAK_IMAGE_INDEX, FOrgImage);

  Result := (not FOrgImage.Empty) and Replace(Bmp);
end;

class function TSushiBreak.Replace(const Bmp: TBitmap): Boolean;
begin
  Result := True;

  try
    FImages.ReplaceMasked(
      BREAK_IMAGE_INDEX,
      Bmp,
      Bmp.Canvas.Pixels[0, Bmp.Height - 1]);
  except
    Result := False;
  end;
end;

class function TSushiBreak.Unregister: Boolean;
begin
  Result := Replace(FOrgImage);
end;

end.
