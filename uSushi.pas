﻿unit uSushi;

interface

implementation

{$R sushi.res}

uses
  ToolsAPI,
  Winapi.Windows, Winapi.Messages,
  System.Classes, System.SysUtils, System.IOUtils,
  Vcl.Graphics, Vcl.Menus, Vcl.Forms,
  USushiBreak;

type
  TSushi = class
  private const
    MESSAGE_HEADER = '[SUSHI] ';
    SUSHI_GROUP_NAME = 'SUSHI';
    RES_SUSHI_ICON = 'SUSHIICON';
    RES_SUSHI_BMP = 'SUSHIBMP';
  private var
    FMenuIcon: TIcon;
    FSushiBmp: TBitmap;
    FSushied: Boolean;
    FMenuItem: TMenuItem;
  public
    procedure SushiClicked(Sender: TObject);
    constructor Create(
      const iNTAServices: INTAServices;
      const iMenuItem: TMenuItem); reintroduce;
    destructor Destroy; override;
  end;

{ TSushi }

constructor TSushi.Create(
  const iNTAServices: INTAServices;
  const iMenuItem: TMenuItem);
begin
  inherited Create;

  FMenuItem := iMenuItem;

  FMenuIcon := TIcon.Create;
  FMenuIcon.LoadFromResourceName(HInstance, RES_SUSHI_ICON);

  FSushiBmp := TBitmap.Create;
  FSushiBmp.LoadFromResourceName(HInstance, RES_SUSHI_BMP);

  FMenuItem.ImageIndex := iNTAServices.ImageList.AddIcon(FMenuIcon);
  FMenuItem.OnClick := SushiClicked;
end;

destructor TSushi.Destroy;
begin
  TSushiBreak.Unregister;

  FSushiBmp.DisposeOf;
  FMenuIcon.DisposeOf;

  inherited;
end;

procedure TSushi.SushiClicked(Sender: TObject);
begin
  FSushied := not FSushied;

  if FSushied then
    FSushied := TSushiBreak.Register(FSushiBmp)
  else
    FSushied := not TSushiBreak.Unregister;
end;

const
  MENU_NAME = 'menuSushi';

  MENU_CAPTION_JP = 'お寿司ブレイク';
  MENU_CAPTION_EN = 'Sushi Break Point';

  MENU_SHORTCUT = 'Shift+Ctrl+Alt+S';

  IDE_MENU_DEST = 'HelpMenu';

var
  MenuSushi: TMenuItem = nil;
  Sushi: TSushi = nil;

procedure Initialize;
var
  NTAServices: INTAServices;
  Caption: String;
begin
  if Supports(BorlandIDEServices, INTAServices, NTAServices) then
  begin
    if GetSystemDefaultLCID = 1041 then
      Caption := MENU_CAPTION_JP
    else
      Caption := MENU_CAPTION_EN;

    MenuSushi :=
      NewItem(
        Caption,
        TextToShortCut(MENU_SHORTCUT),
        False,
        True,
        nil,
        0,
        MENU_NAME);

    Sushi := TSushi.Create(NTAServices, MenuSushi);

    NTAServices.AddActionMenu(IDE_MENU_DEST, nil, NewLine, True, True);
    NTAServices.AddActionMenu(IDE_MENU_DEST, nil, MenuSushi, True, True);
  end;
end;

procedure Finalize;
begin
  Sushi.DisposeOf;
  MenuSushi.DisposeOf;
end;

initialization
  Initialize;

finalization
  Finalize;

end.
