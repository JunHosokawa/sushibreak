#Sushi Break Point

Delphi のブレイクポイントをお寿司（中トロ）にする IDE 拡張です。

##使い方

ソースを落としてビルドしてインストールします。  
すると Help メニューの一番下に「お寿司ブレイク」（日本語環境以外では「Sushi Break Point」）が現れます。  
クリックするとブレイクポイントが中トロになります。  
再度クリックすると元に戻ります。

##謝辞
lynatan さんのこちらのソースのおかげで実装出来ました！
[https://gist.github.com/lynatan/217dbbf0de4db81de60c279eb3171039](https://gist.github.com/lynatan/217dbbf0de4db81de60c279eb3171039)

##著作権
USushiBreak.pas の作成者・著作権者は [@lynatan](https://twitter.com/lynatan) さんです。  
それ以外は [@pik](https://twitter.com/pik) が作成しました。  

Copyright (c) 2016 lynatan  
Copyright (c) 2016 pik  
Released under the MIT license  
http://opensource.org/licenses/mit-license.php